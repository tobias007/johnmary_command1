###Instructions:


Clone repository using:
```
	git clone https://tobias007@bitbucket.org/tobias007/johnmary_command1.git
```
Move to project directory
```
	cd {...}/johnmary_command1
```

Run command in CLI(e.g.Git Bash, Docker Quickstart Terminal):
```
	php console.php johnmary '{text}'
```
for example:
```
	php console.php johnmary 'Lorem ipsum Mary dolor sit John, consectetur adipiscing MARY, sed do eiusmod JOhn incididunt ut maRy et dolore magna aliqua. Ut enim john minim veniam, quis nostrud exercitation mary laboris nisi ut aliquip ex JOHN.'

```
You need to have PHP installed on your operating system.

###Unit Tests


Move to project directory
```
	cd {...}/johnmary_command1
```

Run unit tests using command:
```
 ./vendor/bin/simple-phpunit tests
```

Depending on your operating system you may need to add execution permissions for phpunit. Command for linux:
```
chmod +x ./vendor/bin/simple-phpunit
```