<?php namespace Console;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Console\Command;


class JohnMaryCommand extends Command
{
    public function configure()
    {
        $this   ->setName('johnmary')
                ->setDescription('check if “John” and “Mary” names are found the same number of times inside the provided text')
                ->setHelp('Pass string parameter')
                ->addArgument('text', InputArgument::REQUIRED, 'String to examine');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->checkString($input, $output);
    }
}