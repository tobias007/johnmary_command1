<?php namespace Console;
use Symfony\Component\Console\Command\Command as SymfonyCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Exception\RuntimeException;

/** 
* author:  Tomasz Drzymalski
* purpose: Check if “John” and “Mary” names are found the same number of times inside the provided text
*/
class Command extends SymfonyCommand
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function checkString(InputInterface $input, OutputInterface $output)
    {
        $output->write($this->checkJohnMary($input->getArgument('text')));
    }

    private function checkJohnMary($string)
    {
            if(substr_count(strtoupper($string),"JOHN") == substr_count(strtoupper($string),"MARY")){
                return 1;
            }
            else {
                return 0;
            }
    }
}