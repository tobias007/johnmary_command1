<?php

require_once __DIR__.'/../vendor/autoload.php';
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Command\Command as SymfonyCommand;
use Console\Command;



class CommandTest extends TestCase
{

    public function testCheckJohnMaryCommand()
    {
        $method = new \ReflectionMethod('\Console\Command', 'checkJohnMary');
        $method->setAccessible(true);
        $JohnMaryCmd = new Console\Command();
        $this->assertEquals(1,  $method->invoke($JohnMaryCmd,  'Lorem ipsum Mary dolor sit John, consectetur adipiscing MARY, sed do eiusmod JOhn incididunt ut maRy et dolore magna aliqua. Ut enim john minim veniam, quis nostrud exercitation mary laboris nisi ut aliquip ex JOHN.'));
        $this->assertEquals(0,  $method->invoke($JohnMaryCmd,  'Mary John isi ut JOHN aliquip Mary ex JOHN.'));
        $this->assertEquals(1,  $method->invoke($JohnMaryCmd,  'MaRy ex JOhN.'));
        $this->assertEquals(0,  $method->invoke($JohnMaryCmd,  'MaRy Mar Joh JOh.N'));
    }

}