#!/usr/bin/env php
<?php
require_once __DIR__ . '/vendor/autoload.php';
use Symfony\Component\Console\Application;
use Console\JohnMaryCommand;

/** 
* author: Tomasz Drzymalski
* purpose:  check if “John” and “Mary” names are found the same number of times inside the provided text
*/

$app = new Application();
$app->add(new JohnMaryCommand());
$app->run();